class Student
  attr_reader :first_name
  attr_reader :last_name
  attr_reader :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    if @courses.include?(new_course)
      puts "Already enrolled in course!"
    elsif @courses.any? { |course| course.conflicts_with?(new_course) }
      raise "Conflicts with existing course!"
    else
      @courses << new_course
      new_course.students << self
    end
  end

  def course_load
    dept_credits = Hash.new(0)
    @courses.each do |course|
      dept_credits[course.department] += course.credits
    end
    dept_credits
  end
end
